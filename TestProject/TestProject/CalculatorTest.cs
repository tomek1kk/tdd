using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TDD_Tutorial;

namespace TestProject
{
    [TestClass]
    public class CalculatorTest
    {
        Calculator calculator = new Calculator();
        [TestMethod]
        public void TestEmptyString()
        {
            //Arrange
            string emptyString = "";
            int expectedResult = 0;

            //Act
            int result = calculator.Calculate(emptyString);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void TestSingleValue()
        {
            //Arrange
            string singleValue = "5";
            int expectedResult = 5;

            //Act
            int result = calculator.Calculate(singleValue);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void TestDoubleValue()
        {
            //Arrange
            string doubleValue = "3,75";
            int expectedResult = 78;

            //Act
            int result = calculator.Calculate(doubleValue);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void TestTripleValue()
        {
            //Arrange
            string tripleValue = "3,45,7";
            int expectedResult = 55;

            //Act
            int result = calculator.Calculate(tripleValue);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestNegativeValue()
        {
            //Arrange
            string negativeValueString = "3,-45,7";

            //Act
            int value = calculator.Calculate(negativeValueString);
            
            //Assert
        }

        [TestMethod]
        public void TestOver1000()
        {
            //Arrange
            string over1000String = "4,1233,10";
            int expectedResult = 14;

            //Act
            int result = calculator.Calculate(over1000String);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void TestDifferentChar()
        {
            //Arrange
            string differentCharString = "//#\n4#13#10";
            int expectedResult = 27;

            //Act
            int result = calculator.Calculate(differentCharString);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void TestLongSeparator()
        {
            //Arrange
            string differentCharString = "//[###]\n4###13###10";
            int expectedResult = 27;

            //Act
            int result = calculator.Calculate(differentCharString);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void TestArraySeparator()
        {
            //Arrange
            string differentCharString = "//[###],[,],[???]\n4,13???10";
            int expectedResult = 27;

            //Act
            int result = calculator.Calculate(differentCharString);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

    }
}
