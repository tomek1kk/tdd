﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDD_Tutorial
{
    public class Calculator
    {
        public Calculator() { }
        public int Calculate(string input)
        {
            if (input == "")
                return 0;

            var numbers = GetSplittedValues(input);

            if (numbers.Length >= 1 && numbers.Length <= 3)
            {
                int[] tab = new int[3];
                int sum = 0;
                for (int i = 0; i < numbers.Length; i++)
                {
                    Int32.TryParse(numbers[i], out tab[i]);
                    if (tab[i] < 0)
                        throw new Exception("Negative value");
                    if (tab[i] <= 1000)
                        sum = sum + tab[i];
                }

                return sum;
            }
            return -1;
        }

        private string[] GetSplittedValues(string input)
        {
            List<string> splitCharacters = new List<string>() { "," };
            if (input.Length >= 2 && input.Substring(0, 2) == "//")
            {
                if (input[2] == '[')
                {
                    splitCharacters = GetSplitStrings(input);
                    input = input.Substring(input.LastIndexOf(']') + 1);
                }
                else
                {
                    splitCharacters = new List<string>() { input[2].ToString() };
                    input = input.Substring(3);
                }
            }
            List<string> result = new List<string>();
            MySplit(input, splitCharacters, result);
            return result.ToArray();
        }

        private List<string> GetSplitStrings(string input)
        {
            input = input.Substring(2);
            List<string> result = new List<string>();
            while (input[0] == '[')
            {
                int i = input.IndexOf(']');
                var splitString = input.Substring(1, i - 1);
                result.Add(splitString);
                input = input.Substring(i + 2);
            }
            return result;
        }

        private void MySplit(string input, List<string> splitCharacters, List<string> result)
        {
            bool end = true;
            string[] x = null;
            for (int i = 0; i < splitCharacters.Count; i++)
            {
                if (input.Split(splitCharacters[i]).Length > 1)
                {
                    end = false;
                    x = input.Split(splitCharacters[i]);
                    break;
                }
            }
            if (end == true)
            {
                result.Add(input);
                return;
            }
            for (int i = 0; i < x.Length; i++)
                MySplit(x[i], splitCharacters, result);
        }

        

    }
}
